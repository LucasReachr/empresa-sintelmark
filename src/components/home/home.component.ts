import { Component, OnInit } from '@angular/core';
import {WordpressService} from '../../service/wordpress.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {


  constructor(private wp: WordpressService, ) {}

  ngOnInit(): void {
  }

  irParaOContato(): void {
    window.open('https://recrutamentodigital.reachr.com.br/sintelmark', '_blank');
  }

  irParaOCandidato(): void {
    window.open('https://www.reachr.com.br/#/Vaga/5f4cf6c6-34ad-4fa2-bb70-1d137d74668b', '_blank');
  }


  irParaObancoDeTalentos(): void {
    window.open('https://www.reachr.com.br/#/Vaga/5f4cf6c6-34ad-4fa2-bb70-1d137d74668b', '_blank');
  }

}
