import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { empresaMock } from '../shared/empresa-mock';

@Injectable({
  providedIn: 'root'
})
export class WordpressService {

  constructor(private http: HttpClient) { }

  getPosts(): any {
    return this.http.get(empresaMock.url_blog_posts, { params: {per_page: '3'} });
  }
}
