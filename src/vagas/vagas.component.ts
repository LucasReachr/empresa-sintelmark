import { Component, OnInit, HostListener } from '@angular/core';
import {VagaService} from '../service/vaga.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-vagas',
  templateUrl: './vagas.component.html',
  styleUrls: ['./vagas.component.css'],
  providers: [ VagaService ]
})
export class VagasComponent implements OnInit {

  vagas = [];
  idEmpresa = 'b8b15578-73c7-41c0-9deb-84eb666f1e6c';
  slides: any = [[]];
  CAROUSEL_BREAKPOINT = 768;
  carouselDisplayMode = 'multiple';

  chunk(arr: any, chunkSize: any) {
    const R = [];
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }

  constructor(private vagaService: VagaService, private http: HttpClient) {
    this.getVagas();

  }

  ngOnInit(): void {
    this.vagaService.getVagas(this.idEmpresa).subscribe(vagas => {
      this.vagas = vagas;
      console.log(this.vagas);
    });

    if (window.innerWidth <= this.CAROUSEL_BREAKPOINT) {
      this.carouselDisplayMode = 'single';
    } else {
      this.carouselDisplayMode = 'multiple';
    }
  }

  getVagas() {
    let array;
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    // Request
    const url = 'https://www.reachr.com.br/api/VagaParts/GetVagasEmpresaCanais/';
    this.http.get(url + this.idEmpresa, { headers }).subscribe(data => {
      array = data;
      let i = 0;

      for (i; i < array.length; i++) {
        if (
          ((array[i].Part.nome) && (array[i].Part.tipoVaga === 0 || array[i].Part.tipoVaga === 2))
        ) {
          this.vagas.push(array[i]);
        }
      }
      this.slides = this.chunk(this.vagas, 4);
      console.log(this.vagas);
    });

  }

  @HostListener('window:resize')
  onWindowResize() {
    if (window.innerWidth <= this.CAROUSEL_BREAKPOINT) {
      this.carouselDisplayMode = 'single';
    } else {
      this.carouselDisplayMode = 'multiple';
    }
  }

}
