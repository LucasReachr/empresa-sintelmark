import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Sintelmark';

  constructor(router: Router) {

    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        const tree = router.parseUrl(router.url);
        if (tree.fragment) {
          const element = document.querySelector('#' + tree.fragment);
          if (element) {
            element.scrollIntoView(true);
          }
        }

        gtag('config', 'G-BX237RTZVY',{
          'page_path': event.urlAfterRedirects
        });
      }
    });
  }

  ngOnInit() { }
}
